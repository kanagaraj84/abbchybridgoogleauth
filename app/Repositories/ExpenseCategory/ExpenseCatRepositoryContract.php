<?php
namespace App\Repositories\ExpenseCategory;

interface ExpenseCatRepositoryContract
{
    public function getAllExpenseCat();

    public function find($id);

    public function create($requestData);

    public function destroy($id);
}
