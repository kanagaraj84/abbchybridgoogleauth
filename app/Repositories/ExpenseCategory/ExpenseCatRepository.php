<?php
namespace App\Repositories\ExpenseCategory;

use App\Models\ExpenseCategory;
use App\Repositories\ExpenseCategory\ExpenseCatRepositoryContract;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportsRepository
 * @package App\Repositories\Reports
 */
class ExpenseCatRepository implements ExpenseCatRepositoryContract
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllExpenseCat()
    {
        return ExpenseCategory::pluck('title', 'id');
    }


    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return ExpenseCategory::findOrFail($id);
    }


    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        $reports = ExpenseCategory::create($requestData->all());
        $insertedId = $reports->id;
        return $insertedId;
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        ExpenseCategory::findorFail($id)->delete();
    }
}
