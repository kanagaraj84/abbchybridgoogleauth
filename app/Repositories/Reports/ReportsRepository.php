<?php
namespace App\Repositories\Reports;

use App\Models\Reports;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportsRepository
 * @package App\Repositories\Reports
 */
class ReportsRepository implements ReportsRepositoryContract
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllReports()
    {
        return Reports::pluck('title', 'id');
    }


    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Reports::findOrFail($id);
    }


    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        $reports = Reports::create($requestData->all());
        $insertedId = $reports->id;
        return $insertedId;
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        Reports::findorFail($id)->delete();
    }
}
