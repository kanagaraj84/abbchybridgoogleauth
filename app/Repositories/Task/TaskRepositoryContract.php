<?php
namespace App\Repositories\Task;

interface TaskRepositoryContract
{

    public function find($id);

    public function findMemberTasks($leadId);

    public function countTask($id);

    public function getInvoiceLines($id);

    public function create($requestData);

    public function updateStatus($id, $requestData);
    
    public function updatePercent($id, $requestData);

    public function updateTime($id, $requestData);

    public function updateAssign($id, $requestData);

    public function updateFollowup($id, $requestData);

    public function tasks();

    public function allMarketTasks($leads);
    public function allMarketTasksData($leads);

    public function allITTasks($leads);
    public function allITTasksData($leads);

    public function taskData();

    public function allCompletedTasks();

    public function allPendingTasks();

    public function percantageCompleted();

    public function percantageMarketCompleted($leads);

    public function percantageITCompleted($leads);

    public function createdTasksMothly();

    public function completedTasksMothly();

    public function pendingTasksThisMonth();
    
    public function createdTasksToday();

    public function completedMarketTasksToday($leads);

    public function createdMarketTasksToday($leads);

    public function completedTasksToday();

    public function pendingTasksToday();

    public function completedTasksThisMonth();

    public function completedMarketTasksThisMonth($leads);

    public function totalTimeSpent();

    public function totalOpenAndClosedTasks($id);

    public function allCompletedMarketTasks($leads);

    public function allCompletedMarketTasksData($leads);

    public function allPendingMarketTasks($leads);

    public function allPendingMarketTasksData($leads);

    public function allArticleMarketTasks();

    public function allSNSMarketTasks();

    public function allMonthlyArticleMarketTasks();
    public function allMonthlySNSMarketTasks();

    public function allArticleMarketTasksToday();
    public function allSNSMarketTasksToday();

    public function allCompletedITTasks($leads);

    public function allCompletedITTasksData($leads);

    public function allPendingITTasks($leads);

    public function allPendingITTasksData($leads);

    public function pendingITTasksThisMonth($leads);

    public function completedITTasksToday($leads);

    public function completedITTasksThisMonth($leads);

    public function createdITTasksToday($leads);

    public function completedTasksBetweenDatesCount($sDate, $eDate);

    public function pendingTasksBetweenDatesCount($sDate, $eDate);

    public function allArticleMarketTasksBetweenDates($sDate, $eDate);

    public function allSNSMarketTasksBetweenDates($sDate, $eDate);
    
    public function destroy($id);

}
