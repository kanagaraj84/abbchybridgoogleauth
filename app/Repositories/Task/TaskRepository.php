<?php
namespace App\Repositories\Task;

use App\Models\Task;
use App\Models\Lead;
use Carbon;
use App\Models\Invoice;
use App\Models\InvoiceLine;
use Illuminate\Support\Facades\DB;
use App\Models\Integration;
use App\Models\Activity;
/**
 * Class TaskRepository
 * @package App\Repositories\Task
 */
class TaskRepository implements TaskRepositoryContract
{
    const CREATED = 'created';
    const UPDATED_STATUS = 'updated_status';
    const UPDATED_TIME = 'updated_time';
    const UPDATED_ASSIGN = 'updated_assign';
    const UPDATED_DEADLINE = 'updated_deadline';
    const UPDATED_PERCENTAGE = 'updated_percentage';

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Task::findOrFail($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findMemberTasks($leadId)
    {
        return Task::where('lead_id', $leadId)->get();
    }



    /**
     * @param $id
     * @return mixed
     */
    public function countTask($id)
    {
        return Task::findOrFail($id)->count();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAssignedClient($id)
    {
        $task = Task::findOrFail($id);
        $task->client;
        return $task;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getInvoiceLines($id)
    {
        if (Task::findOrFail($id)->invoice) {
            return Task::findOrFail($id)->invoice->invoiceLines;
        } else {
            return [];
        }
        
    }

    /**
     * @param $requestData
     * @return mixed
     */
    public function create($requestData)
    {
        $lead_id = $requestData->get('lead_id');
        $priority = $requestData->get('priority');
        $input = $requestData = array_merge(
            $requestData->all(),
            ['user_created_id' => auth()->id(),]
        );

        $task = Task::create($input);
        $insertedId = $task->id;
        Session()->flash('flash_message', 'Task successfully added!');
        event(new \App\Events\TaskAction($task, self::CREATED));

        return $insertedId;
    }

    /**
     * @param $id
     * @param $requestData
     */
    public function updateStatus($id, $requestData)
    {
        $task = Task::findOrFail($id); 
        $input =  ['status' => 2]; 
        $task->fill($input)->save();
        $lead = Lead::findOrFail($task->lead_id);
        event(new \App\Events\LeadAction($lead, self::UPDATED_STATUS));
    }


 /**
     * @param $id
     * @param $requestData
     */
    public function updatePercent($id, $requestData)
    {
        $task = Task::findOrFail($id);
        $taskpercent = $requestData->get('taskpercent');
        $input =  ['taskpercent' => $taskpercent];
        $task->fill($input)->save();
        $lead = Lead::findOrFail($task->lead_id);
        event(new \App\Events\LeadAction($lead, self::UPDATED_PERCENTAGE));
    }
    
    /**
     * @param $id
     * @param $request
     */
    public function updateTime($id, $request)
    {
        $task = Task::findOrFail($id);

        $invoice = $task->invoice;
        if(!$invoice) {
            $invoice = Invoice::create([
                'status' => 'draft',
                'client_id' => $task->client->id
            ]);
            $task->invoice_id = $invoice->id;
            $task->save();
        } 

        InvoiceLine::create([
                'title' => $request->title,
                'comment' => $request->comment,
                'quantity' => $request->quantity,
                'type' => $request->type,
                'price' => $request->price,
                'invoice_id' => $invoice->id
        ]);

        event(new \App\Events\TaskAction($task, self::UPDATED_TIME));
    }

    /**
     * @param $id
     * @param $requestData
     */
    public function updateAssign($id, $requestData)
    {
        $task = Task::with('user')->findOrFail($id);
        $input = $requestData->get('user_assigned_id');
        $input = array_replace($requestData->all());
        $task->fill($input)->save();
        $task = $task->fresh();

        event(new \App\Events\TaskAction($task, self::UPDATED_ASSIGN));
    }


    /**
     * @param $id
     * @param $requestData
     */
    public function updateFollowup($id, $requestData)
    {
        $task = Task::findOrFail($id);
        $input = $requestData->all();
        $input = $requestData =
            ['contact_date' => $requestData->contact_date . " " . $requestData->contact_time . ":00",
                'priority' => $requestData->priority];
        $task->fill($input)->save();
        event(new \App\Events\TaskAction($task, self::UPDATED_DEADLINE));
    }

    /**
     * Statistics for Dashboard
     */

    public function tasks()
    {
        return Task::all()->count();
    }

    /**
     * Statistics for Dashboard
     */
    public function allMarketTasks($leads)
    {
        return Task::whereIn('lead_id', $leads)->count();
    }

    /**
     * Statistics for Dashboard
     */
    public function allMarketTasksData($leads)
    {
        return Task::whereIn('lead_id', $leads)->get();
    }

    /**
     * Statistics for Dashboard
     */
    public function allITTasks($leads)
    {
        return Task::whereIn('lead_id', $leads)->count();
    }

    /**
     * Statistics for Dashboard
     */
    public function allITTasksData($leads)
    {
        return Task::whereIn('lead_id', $leads)->get();
    }

    /**
     * @return mixed
     */
    public function allCompletedTasks()
    {
        return Task::where('status', 2)->count();
    }

    /**
     * @return mixed
     */
    public function allPendingTasks()
    {
        return Task::where('status', 1)->count();
    }

    /**
     * @return mixed
     */
    public function allCompletedMarketTasks($leads)
    {
        return Task::where('status', "=",2)
            ->whereIn('lead_id', $leads)
            ->count();
    }

    /**
     * @return mixed
     */
    public function allCompletedMarketTasksData($leads)
    {
        return Task::where('status', "=",2)
            ->whereIn('lead_id', $leads)
            ->get();
    }

    /**
     * @return mixed
     */
    public function allPendingMarketTasks($leads)
    {
        return Task::where('status', "=",1)
            ->whereIn('lead_id', $leads)->count();
    }

    /**
     * @return mixed
     */
    public function allPendingMarketTasksData($leads)
    {
        return Task::where('status', "=",1)
            ->whereIn('lead_id', $leads)->get();
    }

    /**
     * @return mixed
     */
    public function allCompletedITTasks($leads)
    {
        return Task::where('status', "=", 2)
            ->whereIn('lead_id', $leads)
            ->count();
    }

    /**
     * @return mixed
     */
    public function allCompletedITTasksData($leads)
    {
        return Task::where('status', "=", 2)
            ->whereIn('lead_id', $leads)
            ->get();
    }

    /**
     * @return mixed
     */
    public function allPendingITTasks($leads)
    {
        return Task::where('status', "=", 1)
            ->whereIn('lead_id', $leads)->count();
    }


    /**
     * @return mixed
     */
    public function allPendingITTasksData($leads)
    {
        return Task::where('status', "=", 1)
            ->whereIn('lead_id', $leads)->get();
    }


    /**
     * @return mixed
     */
    public function allArticleMarketTasks()
    {
        return Task::where('taskcategory_id', 2)->count();
    }

    /**
     * @return mixed
     */
    public function allSNSMarketTasks()
    {
        return Task::where('taskcategory_id', 1)->count();
    }

    /**
     * @return float|int
     */
    public function percantageCompleted()
    {
        if (!$this->tasks() || !$this->allCompletedTasks()) {
            $totalPercentageTasks = 0;
        } else {
            $totalPercentageTasks = $this->allCompletedTasks() / $this->tasks() * 100;
        }

        return $totalPercentageTasks;
    }


    /**
     * @return float|int
     */
    public function percantageMarketCompleted($leads)
    {
        if (!$this->allMarketTasks($leads) || !$this->allCompletedMarketTasks($leads)) {
            $totalPercentageTasks = 0;
        } else {
            $totalPercentageTasks = $this->allCompletedMarketTasks($leads) / $this->allMarketTasks($leads) * 100;
        }

        return $totalPercentageTasks;
    }

    /**
     * @return float|int
     */
    public function percantageITCompleted($leads)
    {
        if (!$this->allITTasks($leads) || !$this->allCompletedITTasks($leads)) {
            $totalPercentageTasks = 0;
        } else {
            $totalPercentageTasks = $this->allCompletedITTasks($leads) / $this->allITTasks($leads) * 100;
        }

        return $totalPercentageTasks;
    }


    /**
     * @return mixed
     */
    public function createdTasksMothly()
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as month, created_at'))
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
            ->get();
    }

    /**
     * @return mixed
     */
    public function completedTasksMothly()
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as month, updated_at'))
            ->where('status', 2)
            ->groupBy(DB::raw('YEAR(updated_at), MONTH(updated_at)'))
            ->get();
    }

    /**
     * @return mixed
     */
    public function allMonthlyArticleMarketTasks()
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as month, updated_at'))
            ->where('taskcategory_id', 2)
            ->groupBy(DB::raw('YEAR(updated_at), MONTH(updated_at)'))
            ->get();
    }

    /**
     * @return mixed
     */
    public function allMonthlySNSMarketTasks()
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as month, updated_at'))
            ->where('taskcategory_id', 1)
            ->groupBy(DB::raw('YEAR(updated_at), MONTH(updated_at)'))
            ->get();
    }

    /**
     * @return mixed
     */
    public function createdTasksToday()
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->count();
    }

    /**
     * @return mixed
     */
    public function createdITTasksToday($leads)
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->whereIn('lead_id', $leads)->count();
    }


    /**
     * @return mixed
     */
    public function createdMarketTasksToday($leads)
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->whereIn('lead_id', $leads)->count();
    }

    /**
     * @return mixed
     */
    public function completedTasksToday()
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('status', 2)->count();
    }

    /**
     * @return mixed
     */
    public function completedITTasksToday($leads)
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->whereIn('lead_id', $leads)->where('status', 2)->count();
    }


    /**
     * @return mixed
     */
    public function completedMarketTasksToday($leads)
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->whereIn('lead_id', $leads)->where('status', 2)->count();
    }


    /**
     * @return mixed
     */
    public function allArticleMarketTasksToday()
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('taskcategory_id', 2)->count();
    }

    /**
     * @return mixed
     */
    public function allSNSMarketTasksToday()
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('taskcategory_id', 1)->count();
    }

    /**
     * @return mixed
     */
    public function pendingTasksToday()
    {
        return Task::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('status', 1)->count();
    }

    /**
     * @return mixed
     */
    public function completedTasksThisMonth()
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }


    /**
     * @return mixed
     */
    public function completedMarketTasksThisMonth($leads)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->whereIn('lead_id', $leads)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return mixed
     */
    public function pendingTasksThisMonth()
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 1)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return mixed
     */
    public function completedITTasksThisMonth($leads)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->whereIn('lead_id', $leads)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return mixed
     */
    public function pendingITTasksThisMonth($leads)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 1)
            ->whereIn('lead_id', $leads)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }


    /**
     * @return mixed
     */
    public function totalTimeSpent()
    {
        return DB::table('invoice_lines')
            ->select(DB::raw('SUM(quantity)'))
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function totalOpenAndClosedTasks($id)
    {
        $open_tasks = Task::where('status', 1)
        ->where('user_assigned_id', $id)
        ->count();

        $closed_tasks = Task::where('status', 2)
        ->where('user_assigned_id', $id)->count();

        return collect([$closed_tasks, $open_tasks]);
    }

    /**
     * @return mixed
     */
    public function taskData()
    {
        $user = Auth::user();
        $this->roles =$user->roles->first()->name;
        if($this->roles =='administrator'){
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status']
            )->get();
        } else {
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status']
            )->where('user_assigned_id', $id);
        }
        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('lead_id', function ($tasks) {
                return $tasks->lead->title;
            })

            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                return $user->name;
            })

            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<span class="label label-success">Open</span>' : '<span class="label label-danger">Closed</span>';
            })
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function completedTasksBetweenDatesCount($sDate, $eDate)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();
    }

    /**
     * @return mixed
     */
    public function pendingTasksBetweenDatesCount($sDate, $eDate)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 1)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();
    }

    /**
     * @return mixed
     */
    public function allArticleMarketTasksBetweenDates($sDate, $eDate)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('taskcategory_id', 2)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();
    }


    /**
     * @return mixed
     */
    public function allSNSMarketTasksBetweenDates($sDate, $eDate)
    {
        return DB::table('tasks')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('taskcategory_id', 1)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();
    }

    public function destroy($id)
    {
        $user = Task::findorFail($id);
        $user->delete();
    }
}
