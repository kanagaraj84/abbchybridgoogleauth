<?php
namespace App\Repositories\Lead;

interface LeadRepositoryContract
{
    public function find($id);

    public function getAllLeads();

    public function completedLeadsItToday();

    public function create($requestData);

    public function updateStatus($id, $requestData);

    public function updateFollowup($id, $requestData);

    public function updateAssign($id, $requestData);

    public function leads();

    public function itleads();

    public function getAllItLeads();

    public function getAllItLeadscount();

    public function percantageITCompleted();

    public function createdLeadsITToday();

    public function allCompletedITLeads();

    public function allPendingITLead();

    public function allCompletedLeads();

    public function allPendingLeads();

    public function percantageCompleted();

    public function percantageMarketCompleted();

    public function completedLeadsToday();

    public function completedLeadsMarketToday();

    public function pendingLeadsToday();

    public function createdLeadsToday();
    public function createdLeadsMarketToday();

    public function completedLeadsThisMonth();

    public function completedITLeadsThisMonth();

    public function completedLeadsMarketThisMonth();

    public function pendingLeadsThisMonth();

    public function createdLeadsMonthly();

    public function completedLeadsMonthly();

    public function totalOpenAndClosedLeads($id);

    public function getAllMarketingLeads();

    public function totalMarketingLeads();

    public function allCompletedMarketLeads();

    public function allPendingMarketLead();

    public function leadCompletedBetweenDates($sDate, $eDate);

    public function leadPendingBetweenDates($sDate, $eDate);

}
