<?php
namespace App\Repositories\User;
use App\Models\Country;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Support\Facades\Session;
use Gate;
use Datatables;
use Carbon;
use Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRepository
 * @package App\Repositories\User
 */
class UserRepository implements UserRepositoryContract
{

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return User::findOrFail($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllUserswithAdmin()
    {
        return User::all();
    }

    public function getAllcountries()
    {
        return Country::pluck('country_name', 'id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllUsers()
    {
        return User::whereHas(
            'roles', function($q){
            $q->where('name','!=', 'administrato');
        }
        )->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllUsersManagers()
    {
        /* $users = User::whereHas(
             'roles', function($q){
             $q->where('name', 'manager');
         }
         )->get();*/


        $users = User::where(
            'id', '!=','1'
        )->get();


        DB::table('user')->whereNotIn('id',1)
            ->get();

        return $users;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllUsersEmployees()
    {
        $users = User::whereHas(
            'roles', function($q){
            $q->where('name', 'employee');
        }
        )->get();

        return $users;
    }

    /**
     * @return mixed
     */
    public function getAllUsersWithDepartments()
    {
        return User::all()
            ->pluck('nameAndDepartment', 'id');
    }

    /**
     * @return mixed
     */
    public function getAllUsersWithDepartmentsData()
    {
        return DB::table('department_user')->get();
    }



    /**
     * @return int
     */
    public function getAllUsersCount()
    {
        return User::count();
    }

    /**
     * @param $requestData
     * @return static
     */
    public function create($requestData)
    {
        $companyname = Setting::first()->company;
        $filename = null;
        if ($requestData->hasFile('image_path')) {
            if (!is_dir(public_path(). '/images/'. $companyname)) {
                mkdir(public_path(). '/images/'. $companyname, 0777, true);
            }
            $file =  $requestData->file('image_path');

            $destinationPath = public_path(). '/images/'. $companyname;
            $filename = str_random(8) . '_' . $file->getClientOriginalName() ;
            $file->move($destinationPath, $filename);
        }

        $user = New User();
        $user->name = $requestData->name;
        $user->email = $requestData->email;
        $user->password = bcrypt($requestData->password);
        $user->image_path = $filename;
        $user->save();
        $user->roles()->attach($requestData->roles);
        $user->save();
        Session::flash('flash_message', 'User successfully added!'); //Snippet in Master.blade.php
        return $user;
    }

    /**
     * @param $id
     * @param $requestData
     * @return mixed
     */
    public function update($id, $requestData)
    {
        $user = User::findorFail($id);
        $password = bcrypt($requestData->password);
        // $role = $requestData->roles;

        if ($requestData->password == "") {
            $input =  array_replace($requestData->except('password'));
        } else {
            $input =  array_replace($requestData->all(), ['password'=>"$password"]);
        }
        $user->fill($input)->save();
        // $user->roles()->sync([$role]);
        Session::flash('flash_message', 'User successfully updated!');
        return $user;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $user = User::findorFail($id);
        $user->delete();
    }
}
