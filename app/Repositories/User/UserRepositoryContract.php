<?php
namespace App\Repositories\User;

interface UserRepositoryContract
{
    
    public function find($id);
    
    public function getAllUsers();

    public function getAllUserswithAdmin();

    public function getAllUsersWithDepartments();

    public function getAllUsersWithDepartmentsData();

    public function getAllUsersCount();

    public function getAllUsersManagers();

    public function getAllUsersEmployees();

    public function create($requestData);

    public function update($id, $requestData);

    public function destroy($id);
    
    public function getAllcountries();
}
