<?php
namespace App\Repositories\Comment;

use App\Models\Comment;
use Carbon;
use DB;

/**
 * Class CommentRepository
 * @package App\Repositories\Comment
 */
class CommentRepository implements CommentRepositoryContract
{
     

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allComments()
    {
        return Comment::all();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allCompletedEmailIssues()
    {
              return Comment::where('commentable_type', 'App\Models\Topic')->where('status', 1)->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allPendingEmailIssues()
    {
       return Comment::where('commentable_type', 'App\Models\Topic')->where('status', 0)->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allMonthlyPendingEmailIssues()
    {
            return DB::table('comments')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('commentable_type', 'App\Models\Topic')
            ->where('status', 0)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allMonthlyCompletedEmailIssues()
    {
         return DB::table('comments')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('commentable_type', 'App\Models\Topic')
            ->where('status', 1)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allCompletedEmailIssuesToday()
    {
              return Comment::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('status', 1)
            ->where('commentable_type', 'App\Models\Topic')->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allPendingEmailIssuesToday()
    {
    return Comment::whereRaw(
            'date(created_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('commentable_type', 'App\Models\Topic')->where('status', 0)->count();
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $comment = Comment::findorFail($id);
        $comment->delete();        
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allCompletedEmailIssuesBetweenDatesCount($sDate, $eDate)
    {
        return DB::table('comments')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 1)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();

    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function allPendingEmailIssuesBetweenDatesCount($sDate, $eDate)
    {
        return DB::table('comments')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 0)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();

    }
}
