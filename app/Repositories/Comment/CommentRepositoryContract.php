<?php
namespace App\Repositories\Comment;

interface CommentRepositoryContract
{

    public function allComments(); 

    public function destroy($id);

    public function allCompletedEmailIssues();
    public function allPendingEmailIssues();

    public function allMonthlyPendingEmailIssues();
    public function allMonthlyCompletedEmailIssues();

    public function allCompletedEmailIssuesToday();
    public function allPendingEmailIssuesToday();

    public function allCompletedEmailIssuesBetweenDatesCount($sDate, $eDate);
    public function allPendingEmailIssuesBetweenDatesCount($sDate, $eDate);

}
