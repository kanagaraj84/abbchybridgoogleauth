<?php
namespace App\Http\Controllers;

use App\Listeners\ClientActionLog;
use DB;
use Carbon;
use http\Client;
use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use PragmaRX\Google2FA\Google2FA;

define("WITHDRAWALS_ENABLED", true); //Disable withdrawals during maintenance
define("IN_WALLET", true);


class PagesController extends Controller
{

    protected $users;
    protected $request;

    public function __construct(
        UserRepositoryContract $users
    ) {
        $this->users = $users;
        $this->middleware(['auth', '2fa']);
    }

    // function by zelles to modify the number to bitcoin format ex. 0.00120000
    public function satoshitize($satoshitize) {
        return sprintf("%.8f", $satoshitize);
    }

// function by zelles to trim trailing zeroes and decimal if need
    public function satoshitrim($satoshitrim) {
        return rtrim(rtrim($satoshitrim, "0"), ".");
    }

    /**
     * Dashobard view
     * @param $request     *
     * @return mixed
     */
    public function qrgen(Request $request)
    {
        include('qrgen/phpqrcode/qrlib.php');
        $address = $_GET['address'];
        $png =  \QRcode::png($address,false,0,9);
        echo $png; die;
        //echo $png;  die;
    }

    public function reauthenticate(Request $request)
    {
        // get the logged in user
        $user = Auth::user();

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // generate a new secret key for the user
        $user->google2fa_secret = $google2fa->generateSecretKey();

        // save the user
        $user->save();

        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $user->google2fa_secret
        );

        // Pass the QR barcode image to our view.
        return view('google2fa.register', ['QR_Image' => $QR_Image,
            'secret' => $user->google2fa_secret,
            'reauthenticating' => true
        ]);
    }

    /**
     * Dashobard view
     * @param $request     *
     * @return mixed
     */
    public function dashboard(Request $request)
    {
        // get the logged in user
        $user = Auth::user();

       if($user->verified==1 && $user->google2fa_secret==''){
            // initialise the 2FA class
            $google2fa = app('pragmarx.google2fa');

            // generate a new secret key for the user
            $google2fa_secret = $google2fa->generateSecretKey();

           $id = Auth::user()->id;
           $user = User::find($id);
           $user->google2fa_secret = $google2fa_secret;
           $user->save();

            // generate the QR image
            $QR_Image = $google2fa->getQRCodeInline(
                config('app.name'),
                $user->email,
                $google2fa_secret
            );

            // Pass the QR barcode image to our view.
            return view('google2fa.register', ['QR_Image' => $QR_Image,
                'secret' => $google2fa_secret
            ]);
        } else {

            include('jsonRPCClient.php');
            include('classes/Client.php');
            include('classes/User.php');

            $rpc_host = "127.0.0.1";
            $rpc_port = "45251";
            $rpc_user = "ABBCCoinrpc";
            $rpc_pass = "ABBCCoinrpcpassword";

            $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
            $short = "ABBC"; //Coin Short (BTC)
            $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
            $support = "support@abbcfoundation.com"; //Your support eMail
            $hide_ids = array(1); //Hide account from admin dashboard
            $donation_address = ""; //Donation Addressyourwebsite

            $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

            $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
            $uname = Auth::user()->name;
            $noresbal = $client->getBalance($uname);
            $resbalance = $client->getBalance($uname) - $reserve;
            $addressList = $client->getreceivedbyaddress($uname);
            $transactionList = $client->getTransactionList($uname);
            $allAddressList = $client->getAddressList($uname);
            $allchaininfo = $client->getAllinfo();

            $countAddress = count($allAddressList);

            if($countAddress==0){
                $client->getnewaddress($uname);
                header('Location: '.$_SERVER['PHP_SELF']);exit;
            }

            if ($resbalance < 0) {
                $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
            } else {
                $balance = $resbalance;
            }

            return view('pages.dashboard', compact(
                'balance', 'addressList', 'transactionList', 'allAddressList', 'blockchain_tx_url', 'newaddressmsg', 'donation_address', 'allchaininfo'
            ));

        }

    }



    /**
     * Backup Wallet view
     * @param $request     *
     * @return mixed
     */
    public function backupWallet(Request $request)
    {

        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = $filepath = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $uname = Auth::user()->name;
        $noresbal = $client->getBalance($uname);
        $resbalance = $client->getBalance($uname) - $reserve;
        $id = "abbc_".\Auth::user()->id."_wallet";

        if ($resbalance < 0) {
            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
        } else {
            $balance = $resbalance;
        }

        $walletpath = public_path()."/backupWallet/".$id."/";
        $fpath = 'backupWallet/'.$id;
        if (!empty($_POST['jsaction'])) {
            switch ($_POST['jsaction']) {
                case "backupwallet":
                    if (!is_dir(public_path() . '/backupWallet/' . $id)) {
                        mkdir(public_path() . '/backupWallet/' . $id, 0777, true);
                    }
                    $client->getbackupwallet($walletpath);
                    $backupwalletmsg = "Your wallet Backup has been done";
                    $dir = new \DirectoryIterator($walletpath);
                    $i=1;
                    if (file_exists($walletpath)) {
                        foreach ($dir as $fileinfo) {
                            if (!$fileinfo->isDot()) {
                                $filepath = $fpath . "/" . $fileinfo;
                                $i++;
                            }
                        }
                    }
                    break;
            }
        }
        return view('pages.backupwallet', compact(
            'balance', 'addressList', 'newaddressmsg', 'transactionList', 'blockchain_tx_url', 'backupwalletmsg', 'walletpath', 'filepath'
        ));
    }

    /**
     * Backup Wallet view
     * @param $request     *
     * @return mixed
     */
    public function importwallet(Request $request)
    {
        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = $filepath = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $uname = Auth::user()->name;
        $id = "abbc_".\Auth::user()->id."_wallet";
        //$fpath = 'importwallet/'.$id;

        if (!empty($_POST['jsaction'])) {
            switch ($_POST['jsaction']) {
                case "importwallet":
                    //echo realpath($_FILES["importwallet"]["tmp_name"]); die;
                    $filePath = realpath($_FILES["importwallet"]["tmp_name"]);
                    $client->getimportwallet($filePath);
                    $importwalletmsg = "Your wallet Import has been done";
                    break;
            }
        }

        return view('pages.importwallet', compact(
            'balance', 'addressList','blockchain_tx_url', 'importwalletmsg', 'filepath'
        ));
    }


    /**
     * sendcoin view
     * @param $request     *
     * @return mixed
     */
    public function sendabbccoin(Request $request)
    {
        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $error = array('type' => "none", 'message' => "");
        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $uname = Auth::user()->name;
        $noresbal = $client->getBalance($uname);
        $resbalance = $client->getBalance($uname) - $reserve;

        if ($resbalance < 0) {
            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
        } else {
            $balance = $resbalance;
        }

        if (!empty($_POST['jsaction'])) {
            $json = array();
            switch ($_POST['jsaction']) {
                case "withdraw":
                    $json['success'] = false;
                    if (!WITHDRAWALS_ENABLED) {
                        $json['type'] = "withdraw";
                        $json['message'] = "Withdrawals are temporarily disabled";
                    } elseif (empty($_POST['address']) || empty($_POST['amount']) || !is_numeric($_POST['amount'])) {
                        $json['type'] = "withdraw";
                        $json['message'] = "You have to fill all the fields";
                    } elseif ($_POST['amount'] > $balance) {
                        $json['type'] = "withdraw";
                        $json['message'] = "Withdrawal amount exceeds your wallet balance";
                    } else {
                        $withdraw_message = $client->withdraw($uname, $_POST['address'], (float)$_POST['amount']);
                        $json['success'] = true;
                        $json['message'] = "Withdrawal successful";
                    }
                    break;
            }
        }
        return view('pages.sendcoin', compact('balance','blockchain_tx_url','donation_address', 'withdraw_message', 'json'));
    }


    /**
     * list addresses view
     * @param $request     *
     * @return mixed
     */
    public function listaddresses(Request $request)
    {

        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $uname = Auth::user()->name;
        $noresbal = $client->getBalance($uname);
        $resbalance = $client->getBalance($uname) - $reserve;
        $addressList = $client->getAddressList($uname);
        $transactionList = $client->getTransactionList($uname);

        if ($resbalance < 0) {
            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
        } else {
            $balance = $resbalance;
        }

        if (!empty($_POST['jsaction'])) {
            switch ($_POST['jsaction']) {
                case "new_address":
                    $client->getnewaddress($uname);
                    $newaddressmsg = "A new address was added to your wallet";
                    $noresbal = $client->getBalance($uname);
                    $resbalance = $client->getBalance($uname) - $reserve;
                    $addressList = $client->getAddressList($uname);
                    if ($resbalance < 0) {
                        $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
                    } else {
                        $balance = $resbalance;
                    }
                    break;

                case "withdraw":
                    if (!WITHDRAWALS_ENABLED) {
                        $withdrawmsg = "Withdrawals are temporarily disabled";

                    } elseif (empty($_POST['address']) || empty($_POST['amount']) || !is_numeric($_POST['amount'])) {
                        $withdrawmsg = "You have to fill all the fields";
                    } elseif ($_POST['amount'] > $balance) {
                        $withdrawmsg = "Withdrawal amount exceeds your wallet balance. Please note the wallet owner has set a reserve fee of $reserve $short.";
                    } else {
                        $withdrawmsg = $client->withdraw($uname, $_POST['address'], (float)$_POST['amount']);
                        //$withdrawmsg = "Withdrawal successful";
                        $noresbal = $client->getBalance($uname);
                        $resbalance = $client->getBalance($uname) - $reserve;
                        $addressList = $client->getAddressList($uname);

                        if ($resbalance < 0) {
                            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
                        } else {
                            $balance = $resbalance;
                        }
                    }
                    break;

            }
        }

        return view('pages.listaddresses', compact(
            'balance', 'addressList', 'newaddressmsg', 'transactionList', 'blockchain_tx_url', 'donation_address', 'withdrawmsg'
        ));
    }

    /**
     * List transaction history view
     * @param $request     *
     * @return mixed
     */
    public function trans_history(Request $request)
    {

        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $uname = Auth::user()->name;
        $noresbal = $client->getBalance($uname);
        $resbalance = $client->getBalance($uname) - $reserve;
        $addressList = $client->getAddressList($uname);
        $transactionList = $client->getTransactionList($uname);

        if ($resbalance < 0) {
            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
        } else {
            $balance = $resbalance;
        }

        return view('pages.trans_history', compact(
            'balance', 'addressList', 'newaddressmsg', 'transactionList', 'blockchain_tx_url', 'donation_address', 'withdrawmsg'
        ));
    }


    /**
     * Market Cap API Prices view
     * @param $request     *
     * @return mixed
     */
    public function marketcapapiprices()
    {
        // Get data for all coins from Coinmarketcap API
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://s2.coinmarketcap.com/generated/search/quick_search.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        // Replace quotes that might break the json decode
        $output = str_replace("'", "", $output);

        // Decode json data to use as a php array
        $outputdecoded = json_decode($output, true);

        // Decode json data to use as a php array
        $priceArr = json_decode(file_get_contents("https://api.coinmarketcap.com/v1/ticker/"), TRUE);
        return view('pages.marketcapapi', compact(
            'priceArr', 'outputdecoded'
        ));
    }



}
