<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Yajra\Datatables\Datatables;
use App\Models\Role;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Support\Facades\Auth;
define("WITHDRAWALS_ENABLED", true); //Disable withdrawals during maintenance
define("IN_WALLET", true);

class UsersController extends Controller
{
    protected $users;
    protected $settings;
    protected $roles;

    public function __construct(UserRepositoryContract $users)
    {
        $this->users = $users;
        $this->middleware(['auth', '2fa']);
    }

    // function by zelles to modify the number to bitcoin format ex. 0.00120000
    public function satoshitize($satoshitize) {
        return sprintf("%.8f", $satoshitize);
    }


    /**
     * @return mixed
     */
    public function index()
    {
        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";
        $resbalanceArr = array();

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $users = User::all();
        $resbalanceArr = array();
        $i=0;
        foreach ($users as $key=>$val){
            $username = $val['name'];
            $balanceArr[$username]= $client->getBalance($username);
            $resbalanceArr[$username]= $client->getBalance($username);
            $i++;
        }
        $totalbalance = array_sum($balanceArr);
        return view('users.index',compact('users', 'totalbalance', 'balanceArr', 'resbalanceArr'));
    }


    /**
     * @param $id
     * @return mixed
     */
    public function userprofile($id)
    {
        $user = $this->users->find($id);

        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $uname = Auth::user()->name;
        $noresbal = $client->getBalance($uname);
        $resbalance = $client->getBalance($uname) - $reserve;
        $addressList = $client->getreceivedbyaddress($uname);
        $transactionList = $client->getTransactionList($uname);
        $allAddressList = $client->getAddressList($uname);
        $allchaininfo = $client->getAllinfo();

        $countAddress = count($allAddressList);

        if($countAddress==0){
            $client->getnewaddress($uname);
            header('Location: '.$_SERVER['PHP_SELF']);exit;
        }

        if ($resbalance < 0) {
            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
        } else {
            $balance = $resbalance;
        }

        return view('users.userprofile', compact( 'user','balance', 'addressList', 'transactionList',
            'allAddressList', 'blockchain_tx_url', 'newaddressmsg', 'donation_address', 'allchaininfo'));
    }


    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        include('jsonRPCClient.php');
        include('classes/Client.php');
        include('classes/User.php');

        $rpc_host = "127.0.0.1";
        $rpc_port = "45251";
        $rpc_user = "ABBCCoinrpc";
        $rpc_pass = "password";

        $fullname = "ABBC Web"; //Website Title (Do Not include 'wallet')
        $short = "ABBC"; //Coin Short (BTC)
        $blockchain_tx_url = "http://127.0.0.1:3001/tx/"; //Blockchain Url
        $support = "support@abbcfoundation.com"; //Your support eMail
        $hide_ids = array(1); //Hide account from admin dashboard
        $donation_address = $walletpath = $filepath = ""; //Donation Addressyourwebsite

        $reserve = "0"; //This fee acts as a reserve. The users balance will display as the balance in the daemon minus the reserve. We don't reccomend setting this more than the Fee the daemon charges.

        $client = new \AbbcClient($rpc_host, $rpc_port, $rpc_user, $rpc_pass);
        $user = $this->users->find($id);

        $uname = $user->name;
        $noresbal = $client->getBalance($uname);
        $resbalance = $client->getBalance($uname) - $reserve;
        $addressList = $client->getreceivedbyaddress($uname);
        $transactionList = $client->getTransactionList($uname);
        $allAddressList = $client->getAddressList($uname);
        $countAddress = count($allAddressList);
        $allchaininfo = $client->getAllinfo();
        if($countAddress==0){
            $client->getnewaddress($uname);
            header('Location: '.$_SERVER['PHP_SELF']);exit;
        }
        if ($resbalance < 0) {
            $balance = $noresbal; //Don't show the user a negitive balance if they have no coins with us
        } else {
            $balance = $resbalance;
        }
        return view('users.show', compact( 'user','balance', 'addressList', 'transactionList', 'allAddressList', 'blockchain_tx_url', 'newaddressmsg',
            'donation_address', 'allchaininfo' ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $user = $this->users->find($id);
        return view('users.edit', compact('user'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $this->users->update($id, $request);
        Session()->flash('flash_message', 'User successfully updated');
        return redirect()->route('users.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $this->users->destroy($id);
        Session()->flash('flash_message', 'User successfully deleted');
        return redirect()->route('users.index');
    }
}
