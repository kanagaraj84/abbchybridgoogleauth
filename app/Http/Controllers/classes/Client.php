<?php if (!defined("IN_WALLET")) { die("Auth Error!"); } ?>
<?php

//To enable developer mode (no need for an RPC server, replace this file with the snipet at https://gist.github.com/d3e148deb5969c0e4b60

class AbbcClient {
	private $uri;
	private $jsonrpc;

	function __construct($host, $port, $user, $pass)
	{
		$this->uri = "http://" . $user . ":" . $pass . "@" . $host . ":" . $port . "/";
		$this->jsonrpc = new jsonRPCClient($this->uri);
	}

	function getBalance($user_session)
	{
		return $this->jsonrpc->getbalance("abbc(" . $user_session . ")", 6);
	}

	function getAddress($user_session)
	{
		return $this->jsonrpc->getaccountaddress("abbc(" . $user_session . ")");
	}

	function getAddressList($user_session)
	{
		return $this->jsonrpc->getaddressesbyaccount("abbc(" . $user_session . ")");
	}

	function getTransactionList($user_session)
	{
		return $this->jsonrpc->listtransactions("abbc(" . $user_session . ")", 10);
	}

	function getreceivedbyaddress($user_session)
	{
		$allAddresses = $this->jsonrpc->getaddressesbyaccount("abbc(" . $user_session . ")");

		$balancebyAddress = array();
		foreach ($allAddresses as $key=>$value){
			$balancebyAddress[$value] = $this->jsonrpc->getreceivedbyaddress($value);
		}
		return $balancebyAddress;
	}

	function getNewAddress($user_session)
	{
		return $this->jsonrpc->getnewaddress("abbc(" . $user_session . ")");
	}

	function withdraw($user_session, $address, $amount)
	{
		return $this->jsonrpc->sendfrom("abbc(" . $user_session . ")", $address, (float)$amount, 6);
	}

	function getbackupwallet($path)
	{
		return $this->jsonrpc->backupwallet($path);
	}

	function getdumpwallet($path)
	{
		return $this->jsonrpc->dumpwallet($path);
	}

	function getimportwallet($filename)
	{
		return $this->jsonrpc->importwallet($filename);
	}


	function getAllinfo(){
		return $this->jsonrpc->getinfo();
	}

}
?>
