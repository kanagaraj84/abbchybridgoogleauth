<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userinformation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userinformation';

    protected $fillable =
        [
              'id',
            'user_id',
            'join_date',
            'resident_visa_issued_date',
            'passport_no',
            'passport_expiry_date',
            'visa_expiry_date',
            'insurance_start_date',
            'insurance_expiry_date',
            'nationality',
            'dob',
            'created_at',
            'updated_at',
            'emp_status',
            'home_addr',
            'salary',
            'bank_name',
            'bank_account_number',
            'swift_code',
            'sponsor_company',
            'gender'
        ];
}
