<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC HYBRID WALLET
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet" />

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="../assets/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script type="text/javascript" src="../assets/daterangepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <style>
        input.form-control {
            color: #ffff;
        }
        .txlist, .table td  {
            color: #E200E1;
        }
    </style>
</head>

<?php

   // function by zelles to modify the number to bitcoin format ex. 0.00120000
    function satoshitize($satoshitize) {
        return sprintf("%.8f", $satoshitize);
    }

// function by zelles to trim trailing zeroes and decimal if need
    function satoshitrim($satoshitrim) {
        return rtrim(rtrim($satoshitrim, "0"), ".");
    }

    ?>

<body class="">
    <div class="wrapper">



<div class="main-panel">
<!-- Navbar -->
<?php echo $__env->make('layouts.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- End Navbar -->
<div class="content">

    <div class="row">
        <div class="col-lg-8">
            <div class="card card-chart">
                <div class="card-header" style="margin-top: 30px;">
                    <h2 class="card-title" align="left"><i class="tim-icons icon-bullet-list-67"></i> Last 10 transactions
                    </h2>
                </div>

                <table class="table table-bordered table-striped" id="txlist">
                    <thead>
                    <tr>
                        <td nowrap="">Date</td>
                        <td nowrap="">Address</td>
                        <td nowrap="">Type</td>
                        <td nowrap="">Status</td>
                        <td nowrap="">Amount</td>
                        <td nowrap="">Fee</td>
                        <td nowrap="">Confs</td>
                        <td nowrap="">Info</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $bold_txxs = "";
                    $transcount = count($transactionList);
                    if($transcount>0){
                        foreach($transactionList as $transaction)
                        {
                            $fee = isset($transaction['fee']) ? $transaction['fee'] : '0.0000';
                            $status = isset($transaction['confirmations']) && ($transaction['confirmations']>6) ? 'Completed' : 'Pending';
                            if($transaction['category']=="send") { $tx_type = '<b style="color: #FF0000;">Sent</b>'; } else { $tx_type = '<b style="color: #01DF01;">Received</b>'; }
                            echo '<tr>
                           <td>'.date('n/j/Y h:i a',$transaction['time']).'</td>
                           <td>'.$transaction['address'].'</td>
                           <td>'.$tx_type.'</td>
                           <td>'.$status.'</td>
                           <td>'.abs($transaction['amount']).' ABBC</td>
                            <td>'.$fee.'</td>
                           <td>'.$transaction['confirmations'].'</td>
                           <td><a href="' . $blockchain_tx_url,  $transaction['txid'] . '" target="_blank">Info</a></td>
                        </tr>';
                        }
                    } else {
                              echo '<tr> <td colspan="8">No Records Found</td></tr>';
                           } ?>
                    </tbody>
                </table>

            </div></div>
    </div>

</div>
</div>

<footer class="footer">
<div class="container-fluid">
<div class="copyright">
    ©
    <script>
        document.write(new Date().getFullYear())
    </script>
    <a href="javascript:void(0)" target="_blank">ABBC Hrbrid Wallet</a>
</div>
</div>
</footer>
</div>
</div>
    <!--   Core JS Files   -->
    <script src="../assets/js/core/jquery.min.js"></script>
    <script src="../assets/js/core/popper.min.js"></script>
    <script src="../assets/js/core/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Place this tag in your head or just before your close body tag. -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Chart JS -->
    <script src="../assets/js/plugins/chartjs.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="../assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
    <!-- Black Dashboard DEMO methods, don't include it in your project! -->
    <script src="../assets/demo/demo.js"></script>


    <script type="text/javascript">
        var blockchain_tx_url = "<?=$blockchain_tx_url?>";
        $("#withdrawform input[name='action']").first().attr("name", "jsaction");
        $("#newaddressform input[name='action']").first().attr("name", "jsaction");
        $("#pwdform input[name='action']").first().attr("name", "jsaction");
        $("#donate").click(function (e){
            $("#donateinfo").show();
            $("#withdrawform input[name='address']").val("<?=$donation_address?>");
            $("#withdrawform input[name='amount']").val("0.01");
        });
        $("#withdrawform").submit(function(e)
        {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                {
                    url : formURL,
                    type: "POST",
                    data : postData,
                    success:function(data, textStatus, jqXHR)
                    {
                        var json = $.parseJSON(data);
                        if (json.success)
                        {
                            $("#withdrawform input.form-control").val("");
                           // $("#withdrawmsg").text(json.message);
                            $("#withdrawmsg").css("color", "green");
                            $("#withdrawmsg").show();
                            updateTables(json);
                        } else {
                           // $("#withdrawmsg").text(json.message);
                            $("#withdrawmsg").css("color", "red");
                            $("#withdrawmsg").show();
                        }
                        if (json.newtoken)
                        {
                            $('input[name="token"]').val(json.newtoken);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        //ugh, gtfo
                    }
                });
            e.preventDefault();
        });
        $("#newaddressform").submit(function(e)
        {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                {
                    url : formURL,
                    type: "POST",
                    data : postData,
                    success:function(data, textStatus, jqXHR)
                    {
                        var json = $.parseJSON(data);
                        if (json.success)
                        {
                            $("#newaddressmsg").text(json.message);
                            $("#newaddressmsg").css("color", "green");
                            $("#newaddressmsg").show();
                            updateTables(json);
                        } else {
                            $("#newaddressmsg").text(json.message);
                            $("#newaddressmsg").css("color", "red");
                            $("#newaddressmsg").show();
                        }
                        if (json.newtoken)
                        {
                            $('input[name="token"]').val(json.newtoken);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        //ugh, gtfo
                    }
                });
            e.preventDefault();
        });
        $("#pwdform").submit(function(e)
        {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                {
                    url : formURL,
                    type: "POST",
                    data : postData,
                    success:function(data, textStatus, jqXHR)
                    {
                        var json = $.parseJSON(data);
                        if (json.success)
                        {
                            $("#pwdform input.form-control").val("");
                            $("#pwdmsg").text(json.message);
                            $("#pwdmsg").css("color", "green");
                            $("#pwdmsg").show();
                        } else {
                            $("#pwdmsg").text(json.message);
                            $("#pwdmsg").css("color", "red");
                            $("#pwdmsg").show();
                        }
                        if (json.newtoken)
                        {
                            $('input[name="token"]').val(json.newtoken);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        //ugh, gtfo
                    }
                });
            e.preventDefault();
        });

        function updateTables(json)
        {
            $("#balance").text(json.balance.toFixed(8));
            $("#alist tbody tr").remove();
            for (var i = json.addressList.length - 1; i >= 0; i--) {
                $("#alist tbody").prepend("<tr><td>" + json.addressList[i] + "</td></tr>");
            }
            $("#txlist tbody tr").remove();
            for (var i = json.transactionList.length - 1; i >= 0; i--) {
                var tx_type = '<b style="color: #01DF01;">Received</b>';
                if(json.transactionList[i]['category']=="send")
                {
                    tx_type = '<b style="color: #FF0000;">Sent</b>';
                }
                $("#txlist tbody").prepend('<tr> \
               <td>' + moment(json.transactionList[i]['time'], "X").format('l hh:mm a') + '</td> \
               <td>' + json.transactionList[i]['address'] + '</td> \
               <td>' + tx_type + '</td> \
               <td>' + Math.abs(json.transactionList[i]['amount']) + '</td> \
               <td>' + (json.transactionList[i]['fee']?json.transactionList[i]['fee']:'') + '</td> \
               <td>' + json.transactionList[i]['confirmations'] + '</td> \
               <td><a href="' + blockchain_tx_url.replace("%s", json.transactionList[i]['txid']) + '" target="_blank">Info</a></td> \
            </tr>');
            }
        }
    </script>


<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



            $('.fixed-plugin a').click(function(event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

    // we simulate the window Resize so the charts will get updated in realtime.
    var simulateWindowResize = setInterval(function() {
    window.dispatchEvent(new Event('resize'));
    }, 180);

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(function() {
    clearInterval(simulateWindowResize);
    }, 1000);
    });

    $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
        var $btn = $(this);

        if (white_color == true) {

            $('body').addClass('change-background');
            setTimeout(function() {
                $('body').removeClass('change-background');
                $('body').removeClass('white-content');
            }, 900);
            white_color = false;
        } else {

            $('body').addClass('change-background');
            setTimeout(function() {
                $('body').removeClass('change-background');
                $('body').addClass('white-content');
            }, 900);

            white_color = true;
        }


    });

    $('.light-badge').click(function() {
        $('body').addClass('white-content');
    });

    $('.dark-badge').click(function() {
        $('body').removeClass('white-content');
    });
});
});
</script>
</body>
</html>
<?php echo $__env->make('layouts.mastersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>