


<div class="form-group">
   <h4 class="card-title">Name</h4>
    <?php echo Form::text('name', null, ['class' => 'form-control','style' => 'color:#ED00D3;' ]); ?>

</div>

<div class="form-group">
    <h4 class="card-title">Mail</h4>
    <?php echo Form::email('email', null, ['class' => 'form-control','style' => 'color:#ED00D3;']); ?>

</div>

<div class="form-group">
    <h4 class="card-title">Password</h4>
    <?php echo Form::password('password', ['class' => 'form-control','style' => 'color:#ED00D3;']); ?>

</div>
<div class="form-group">
    <h4 class="card-title">Confirm password</h4>
    <?php echo Form::password('password_confirmation', ['class' => 'form-control','style' => 'color:#ED00D3;']); ?>

</div>




<?php echo Form::submit($submitButtonText, ['class' => 'btn btn-primary']); ?>

