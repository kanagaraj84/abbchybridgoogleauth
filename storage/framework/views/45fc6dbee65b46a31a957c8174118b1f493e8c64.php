<?php $__env->startSection('content'); ?>

<style type="text/css">
    body{
        margin-top: 100px;
        background-color: #1d1e2d;
    }

    .panel-default{
        border-style: none;
    }

    .panel-body{
        background-color: #27293b;
        color: #f7f7f7;
        border-style: none;
    }

    .btn-primary {
        color: #ffffff;
        background-color: #e14eca;
        border-color: #e14eca;
        box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);
    }

    .btn-primary:hover {
        color: #ffffff;
        background-color: #db2dc0;
        border-color: #d725bb;
    }

    .btn-primary:focus,
    .btn-primary.focus {
        box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08), 0 0 0 0 rgba(225, 78, 202, 0.5);
    }

    .btn-primary.disabled,
    .btn-primary:disabled {
        color: #ffffff;
        background-color: #e14eca;
        border-color: #e14eca;
    }

    .btn-primary:not(:disabled):not(.disabled):active,
    .btn-primary:not(:disabled):not(.disabled).active,
    .show>.btn-primary.dropdown-toggle {
        color: #ffffff;
        background-color: #d725bb;
        border-color: #cd23b2;
    }

    .btn-primary:not(:disabled):not(.disabled):active:focus,
    .btn-primary:not(:disabled):not(.disabled).active:focus,
    .show>.btn-primary.dropdown-toggle:focus {
        box-shadow: none, 0 0 0 0 rgba(225, 78, 202, 0.5);
    }

    .btn-link{
        color: white;
    }

    .btn-link:hover {
        color: #cd23b2;
    }
    h3{
        color: #f7f7f7;
        margin-bottom: 20px;
    }
</style>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Set up Google Authenticator</div>

                    <div class="panel-body" style="text-align: center;">
                        <p>Set up your two factor authentication by scanning the barcode below. Alternatively, you can use the code <?php echo e($secret); ?></p>
                        <div>
                            <img src="<?php echo e($QR_Image); ?>">
                        </div>
                        <?php if(!@$reauthenticating): ?> 
                        <p>You must set up your Google Authenticator app before continuing. You will be unable to login otherwise</p>
                        <div>
                            <a href="/logout"><button class="btn-primary">Complete Registration</button></a>
                        </div>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>