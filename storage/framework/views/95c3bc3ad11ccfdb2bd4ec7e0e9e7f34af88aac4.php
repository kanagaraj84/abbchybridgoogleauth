<nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle d-inline">
                <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navigation">
        <ul class="navbar-nav ml-auto">

            <li class="dropdown nav-item">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <div class="photo">
                        <img src="/<?php echo e(auth()->user()->avatar); ?>" alt="Profile Photo">
                    </div>
                    <b class="caret d-none d-lg-block d-xl-block"></b>
                    <p class="d-lg-none">
                        Log out
                    </p>
                </a>
                <ul class="dropdown-menu dropdown-navbar">
                    <?php if(auth()->user()->id==1){ ?>
                    <li class="nav-link">
                        <a href="<?php echo e(route('users.index')); ?>"
                           class="nav-item dropdown-item">All Users</a>
                    </li>
                    <?php } ?>
                    <li class="nav-link">
                    <a href="/userprofile/<?php echo e(auth()->user()->id); ?>" class="nav-item dropdown-item">Profile</a>
                   </li>

                    <li class="dropdown-divider"></li>
                    <li class="nav-link">
                        <a href="<?php echo e(route('logout')); ?>" class="nav-item dropdown-item">Log out</a>
                    </li>
                </ul>
            </li>
            <li class="separator d-lg-none"></li>
        </ul>
    </div>
</div>


</nav>


