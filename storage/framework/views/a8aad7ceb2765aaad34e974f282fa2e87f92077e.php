<div class="sidebar">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
  -->
  <div class="sidebar-wrapper">
    <div class="logo">
        <a href="javascript:void(0)" class="simple-text logo-mini">
            <img src="<?php echo e(asset('images/abbc_logo.png')); ?>" width="100px"
      alt="">
        </a>
        <a href="/dashboard" class="simple-text logo-normal">
            ABBC HYBRID WALLET
        </a>
    </div>


      <ul class="nav">
          <li>
              <a href="<?php echo e(route('dashboard')); ?>">
                  <i class="tim-icons icon-chart-pie-36"></i>
                  <p style="font-size: 13px; font-weight: 500;">Dashboard</p>
              </a>
          </li>

          <li>
              <a href="<?php echo e(route('sendcoin')); ?>">
                  <i class="tim-icons icon-send"></i>
                  <p style="font-size: 13px; font-weight: 500;">Send</p>
              </a>
          </li>

          <li>
              <a href="<?php echo e(route('backupwallet')); ?>">
                  <i class="tim-icons icon-wallet-43"></i>
                  <p style="font-size: 13px; font-weight: 500;">Backup</p>
              </a>
          </li>


          <li>
              <a href="<?php echo e(route('impwallet')); ?>">
                  <i class="tim-icons icon-cloud-upload-94"></i>
                  <p style="font-size: 13px; font-weight: 500;">Import</p>
              </a>
          </li>

          <li>
              <a href="<?php echo e(route('trans_history')); ?>">
                  <i class="tim-icons icon-bullet-list-67"></i>
                  <p style="font-size: 13px; font-weight: 500;">Transaction History</p>
              </a>
          </li>

          <li>
              <a href="<?php echo e(route('marketcapapi')); ?>">
                  <i class="tim-icons icon-money-coins"></i>
                  <p style="font-size: 13px; font-weight: 500;">CoinMarket Cap</p>
              </a>
          </li>


      </ul>

        </div>
    </div>
