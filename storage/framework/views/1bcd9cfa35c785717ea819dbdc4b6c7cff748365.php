<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC HYBRID WALLET
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet" />

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="../assets/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script type="text/javascript" src="../assets/daterangepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <style>
        input.form-control {
            color: #ffff;
        }
        .table td {
            color: #E200E1;
        }
    </style>
</head>

<?php
// function by zelles to modify the number to bitcoin format ex. 0.00120000
function satoshitize($satoshitize) {
    return sprintf("%.8f", $satoshitize);
}

// function by zelles to trim trailing zeroes and decimal if need
function satoshitrim($satoshitrim) {
    return rtrim(rtrim($satoshitrim, "0"), ".");
}

$i = $amount = 0;
$sendArr = $receiveArr = array();
if($allAddressList){

    foreach($transactionList as $transaction)
    {
        $trans_address = $transaction['address'];
        $amount = abs($transaction['amount']);

        if($transaction['category']=="send") {
            $sendArr[$i]['type'] = 'send';
            $sendArr[$i]['address'] = $trans_address;
            $sendArr[$i]['amount'] = $amount;
        }
        $i++;
    }
}

foreach ($addressList as $address=>$value)
{
    if($value!=0){
        $receiveArr[$i]['address'] = $address;
        $receiveArr[$i]['amount'] = $value;
    }
    $i++;
}

$addressCount = isset($allAddressList) && (is_array($allAddressList)) ? count($allAddressList) : '';
$transactionCount =  isset($transactionList) && (is_array($transactionList)) ? count($transactionList) : '';
$sendcount = count($sendArr);
$receivecount = count($receiveArr);
?>

<body class="">
<div class="wrapper">

    

    <div class="main-panel">
        <!-- Navbar -->
    <?php echo $__env->make('layouts.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- End Navbar -->
     <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <h3>Hi <?php echo e(Auth::user()->name); ?> </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5">
                <div class="card card-chart">
                    <div class="card-header" style="margin: 30px;">
                        <h2 class="card-title" align="center"><i class="tim-icons icon-money-coins"></i> Current balance</h2>
                    </div>
                    <div class="card-body">
                        <div class="info-box-content">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <div class="card-header" style="margin-bottom: 30px;">
                                        <h4 class="card-title" align="center"><?php echo satoshitize($balance); ?> ABBC </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        <div class="col-lg-8">
            <div class="card card-chart">
                <div class="card-header" style="margin-top: 30px;">
                    <h2 class="card-title" align="left"><i class="tim-icons tim-icons icon-bullet-list-67"></i> Address</h2>
                </div>
                <p id="newaddressmsg"><?php echo $newaddressmsg = isset($newaddressmsg) ? $newaddressmsg : ''; ?></p>
                <table class="table table-bordered table-striped" id="alist">
                    <thead>
                    <tr>
                        <td>Address:</td>
                        <td>QR Code:</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(is_array($allAddressList)){
                        foreach ($allAddressList as $address)
                        {
                        echo "<tr><td>".$address."</t>";?>
                        <td><a href="qrgen/?address=<?php echo $address;?>">
                                <img src="qrgen/?address=<?php echo $address;?>" alt="QR Code"></a></td><tr>
                        <?php
                        }
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>

            <canvas id="chartLinePurple" style="display: none"></canvas>

        <div class="row">
            <div class="col-lg-8">
                <div class="card card-chart">
                    <div class="card-header">
                        <a href="#"><h5 class="card-title">TRANSACTION HISTORY</h5></a>
                        <div class="card-body">
                            <div class="chart-area">
                                <input type="hidden" id="allchartlinegreenvalues" value="<?php echo e($sendcount); ?>,<?php echo e($receivecount); ?>" />
                                <canvas id="chartLineGreen"></canvas>
                                <div class="clearfix"></div> <p>&nbsp;&nbsp;</p>
                            </div>
                            <p>&nbsp;&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       <?php if($receivecount>0){ ?>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card card-chart">
                        <div class="card-header" style="margin-top: 30px;">
                            <h2 class="card-title" align="left"><i class="tim-icons tim-icons icon-bullet-list-67"></i> Received transactions</h2>
                        </div>

                        <table class="table table-bordered table-striped" id="alist">
                            <thead>
                            <tr>
                                <td>Address</td>
                                <td>Amount</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($addressList as $address=>$value)
                            {
                                if($value!=0){
                                    echo "<tr><td>".$address."</td><td>".satoshitize($value). " ABBC </td> <tr>";
                                }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>

            <?php
            if((count($sendArr)>0)) { ?>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card card-chart">
                        <div class="card-header" style="margin-top: 30px;">
                            <h2 class="card-title" align="left"><i class="tim-icons tim-icons icon-bullet-list-67"></i> Send transactions</h2>
                        </div>

                        <table class="table table-bordered table-striped" id="alist">
                            <thead>
                            <tr>
                                <td>Address</td>
                                <td>Amount</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($sendArr as $key=>$value)
                            {
                                if($value!=0){
                                    echo "<tr><td>".$value['address']."</td><td>".satoshitize($value['amount'])." ABBC </td> <tr>";
                                }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
    </div>



    <footer class="footer">
        <div class="container-fluid">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="javascript:void(0)" target="_blank">ABBC Hrbrid Wallet</a>
            </div>
        </div>
    </footer>
</div>
</div>
<!--   Core JS Files   -->
<script src="../../assets/js/core/jquery.min.js"></script>
<script src="../../assets/js/core/popper.min.js"></script>
<script src="../../assets/js/core/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../../assets/demo/demo.js"></script>


<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



            $('.fixed-plugin a').click(function(event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

// we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

// we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function() {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function() {
                $('body').removeClass('white-content');
            });
        });
    });

</script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/jquery.dataTables.min.js')); ?>"></script>
<script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>

</body>
</html>
<?php echo $__env->make('layouts.mastersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>