<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC HYBRID WALLET
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../../assets/css/nucleo-icons.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="../../assets/css/black-dashboard.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../../assets/demo/demo.css" rel="stylesheet" />

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="../assets/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/daterangepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

</head>

<?php
// function by zelles to modify the number to bitcoin format ex. 0.00120000
function satoshitize($satoshitize) {
    return sprintf("%.7f", $satoshitize);
}

// function by zelles to trim trailing zeroes and decimal if need
function satoshitrim($satoshitrim) {
    return rtrim(rtrim($satoshitrim, "0"), ".");
}

$i = $j = $amount = 0;
$sendArr = $receiveArr = $allReceiveArr = array();
if($allAddressList){

    foreach($transactionList as $transaction)
    {
        $trans_address = $transaction['address'];
        $amount = abs($transaction['amount']);

        if($transaction['category']=="send") {
            $sendArr[$i]['type'] = 'send';
            $sendArr[$i]['address'] = $trans_address;
            $sendArr[$i]['amount'] = $amount;
        } else {
            $allReceiveArr[$i]['type'] = 'receive';
            $allReceiveArr[$i]['address'] = $trans_address;
            $allReceiveArr[$i]['amount'] = $amount;
        }
        $i++;
    }
}


foreach ($addressList as $address=>$value)
{
   if($value!=0){
        $receiveArr[$j]['address'] = $address;
        $receiveArr[$j]['amount'] = $value;
   }
    $j++;
}

$addressCount = isset($allAddressList) && (is_array($allAddressList)) ? count($allAddressList) : '';
$transactionCount =  isset($transactionList) && (is_array($transactionList)) ? count($transactionList) : '';

$sendcount = count($sendArr);
$receivecount = count($allReceiveArr);
?>

<body class="">
<div class="wrapper">

    

    <div class="main-panel">
        <!-- Navbar -->
    <?php echo $__env->make('layouts.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- End Navbar -->
     <div class="content">

        <div class="row">
            <div class="col-lg-12">
                <h3>Hi <?php echo e(Auth::user()->name); ?> </h3>
            </div>
        </div>

     <div class="row">
         <div class="col-md-4 col-4 py-1">
             <div class="card">
                 <div class="card-header">
                     <h6 class="card-title text-center">ABBC Coin</h6>
                 </div>
                 <div class="card-content collapse show">
                     <div class="card-body">

                         <div class="text-center row clearfix mb-2">
                             <div class="col-md-6 col-12 py-1 text-center">
                                 <p class="mt-0 text-capitalize">
                                     <a href="javascript:void(0)" class="simple-text logo-mini">
                                         <img src="<?php echo e(asset('images/abbc_logo.png')); ?>" width="111"
                                              alt="">
                                     </a></p>
                             </div>

                             <div class="col-md-6 col-12 py-1 text-center">
                                 <?php
                                 if(is_array($allAddressList)){
                                 foreach ($allAddressList as $address)
                                 { ?>
                                 <a href="qrgen/?address=<?php echo $address;?>">
                                     <img src="qrgen/?address=<?php echo $address;?>" width="111" alt="QR Code"></a>
                                 <?php
                                 }
                                 } ?>
                             </div>
                         </div>
                         <div class="card-header">
                             <h6 class="card-title text-center">Balance</h6>
                         </div>
                         <h3 class="text-center"><?php echo e(satoshitize($balance)); ?> ABBC</h3>
                     </div>
                 <div class="table-responsive" style="overflow: hidden">
                     <table class="table table-de mb-0">
                         <tbody>
                     <?php
                     if(is_array($allchaininfo))
                            {
                             foreach ($allchaininfo as $key=>$value)
                             {
                                 if($key=='walletversion'){
                                    echo '<tr><td>Wallet Version</td> <td><i class="icon-layers"></i> '.$value.'</td></tr>';
                                 } elseif($key=='blocks'){
                                    echo '<tr><td>Total Blocks</td> <td><i class="icon-layers"></i> '.$value.'</td></tr>';
                                 } elseif($key=='moneysupply'){
                                     echo '<tr><td>Money supply</td> <td><i class="icon-layers"></i> '.$value.'</td></tr>';
                                 }  elseif($key=='difficulty'){
                                    echo '<tr><td>Proof of Work</td> <td><i class="icon-layers"></i> '.$value['proof-of-work'].'</td></tr>';
                                    echo '<tr><td>Proof of Stake</td> <td><i class="icon-layers"></i> '.$value['proof-of-stake'].'</td></tr>';
                                }
                             }
                            } ?>
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>

         <div class="col-md-8 col-8 py-1">
         <table class="table table-hover wallet-table-th d-none d-md-block" id="wallet-table">
             <thead>
             <tr>
                 <th><div class="col-md-2 col-12 py-1 text-center">
                         <p class="mt-0 text-capitalize">Date</p>
                     </div></th>
                 <th><div class="col-md-4 col-12 py-1 text-center">
                         <p class="mt-0 text-capitalize">Address</p>
                     </div></th>
                 <th><div class="col-md-4 col-12 py-1 text-left">
                         <p class="mt-0 text-capitalize">Amount</p>
                     </div></th>
                 <th> <div class="col-md-2 col-12 py-1 text-center">
                         <p class="mt-0 text-capitalize">Transect</p>
                     </div></th>
             </tr>

             </thead>
             <tbody>
                 <?php
                 $transcount = count($transactionList);
                 if($transcount>0){
                 foreach($transactionList as $transaction)
                 { ?>

                     <tr>
                         <td>
                            <div class="col-12 py-1 text-left  "> <h6><?php echo e(date('n/j/Y h:i a',$transaction['time'])); ?></h6></div>
                         </td>
                         <td>
                             <div class="col-md-12 col-12 py-1 ">
                                 <div class="media">
                                     <i class="cc BTC mr-2 font-large-2 warning"></i>
                                     <div class="media-body">
                                         <p class="text-muted mb-0 font-small-3 wallet-address card"><?php echo e($transaction['address']); ?></p>
                                     </div>
                                 </div>
                             </div>
                         </td>
                         <td>
                             <div class="col-md-12 text-left"><h6><?php echo e(satoshitize($transaction['amount'])); ?> ABBC</h6></div>
                         </td>
                         <td>
                            <div class="col-12 py-1 text-center">
                                <?php if($transaction['category']=="send") {
                                    $tx_type = 'Deposit';
                                } else {
                                    $tx_type = 'Withdraw';
                                } ?>
                                 <a href="#" class="line-height-3"><?php echo e($tx_type); ?></a>
                             </div>
                         </td>
                     </tr>

             <?php }
             } else { ?>
                     <tr>
                         <td>&nbsp;</td>
                         <td width="30%"><h6>No Records found</h6></td>
                         <td>&nbsp;</td>
                         <td>&nbsp;</td>
                     </tr>
            <?php } ?>
                 </tbody>
             </table>
         </div>
     </div>

        <div class="row">
            <canvas id="chartLinePurple" style="display: none"></canvas>
            <div class="col-lg-12">
                <div class="card card-chart">
                    <div class="card-header">
                        <a href="#"><h5 class="card-title">TRANSACTION HISTORY</h5></a>
                        <div class="card-body">
                            <div class="chart-area">
                                <input type="hidden" id="allchartlinegreenvalues" value="<?php echo e($sendcount); ?>,<?php echo e($receivecount); ?>" />
                                <canvas id="chartLineGreen"></canvas>
                                <div class="clearfix"></div> <p>&nbsp;&nbsp;</p>
                            </div>
                            <p>&nbsp;&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="javascript:void(0)" target="_blank">ABBC Hrbrid Wallet</a>
            </div>
        </div>
    </footer>
</div>
</div>
<!--   Core JS Files   -->
<script src="../../assets/js/core/jquery.min.js"></script>
<script src="../../assets/js/core/popper.min.js"></script>
<script src="../../assets/js/core/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<!-- Chart JS -->
<script src="../../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../../assets/demo/demo.js"></script>


<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



            $('.fixed-plugin a').click(function(event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

// we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

// we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function() {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function() {
                $('body').removeClass('white-content');
            });
        });
    });


    $(function () {
        $('#wallet-table').DataTable({
            "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
        })
    });

</script>


<script type="text/javascript" src="<?php echo e(URL::asset('js/jquery.dataTables.min.js')); ?>"></script>
<script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>

</body>
</html>
<?php echo $__env->make('layouts.mastersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>