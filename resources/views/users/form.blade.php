{{--<div>
    {{ Form::label('image_path', __('Image'), ['class' => 'control-label','style' => 'color:#fff;']) }}
    {!! Form::file('image_path',  null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>--}}


<div class="form-group">
   <h4 class="card-title">Name</h4>
    {!! Form::text('name', null, ['class' => 'form-control','style' => 'color:#ED00D3;' ]) !!}
</div>

<div class="form-group">
    <h4 class="card-title">Mail</h4>
    {!! Form::email('email', null, ['class' => 'form-control','style' => 'color:#ED00D3;']) !!}
</div>

<div class="form-group">
    <h4 class="card-title">Password</h4>
    {!! Form::password('password', ['class' => 'form-control','style' => 'color:#ED00D3;']) !!}
</div>
<div class="form-group">
    <h4 class="card-title">Confirm password</h4>
    {!! Form::password('password_confirmation', ['class' => 'form-control','style' => 'color:#ED00D3;']) !!}
</div>

{{--<div class="form-group">
    <h4 class="card-title">Assign role</h4>
    {!! Form::select('roles', $roles, isset($user->role->role_id) ? $user->role->role_id : null, ['class' => 'form-control','style' => 'color:#ED00D3;']) !!}
</div>--}}


{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
