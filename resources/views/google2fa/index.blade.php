@extends('layouts.app')

@section('content')

<style type="text/css">
    body{
        margin-top: 100px;
        background-color: #1d1e2d;
    }

    .panel-default{
        border-style: none;
    }

    .panel-body{
        background-color: #27293b;
        color: #f7f7f7;
        border-style: none;
    }

    .btn-primary {
        color: #ffffff;
        background-color: #e14eca;
        border-color: #e14eca;
        box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);
    }

    .btn-primary:hover {
        color: #ffffff;
        background-color: #db2dc0;
        border-color: #d725bb;
    }

    .btn-primary:focus,
    .btn-primary.focus {
        box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08), 0 0 0 0 rgba(225, 78, 202, 0.5);
    }

    .btn-primary.disabled,
    .btn-primary:disabled {
        color: #ffffff;
        background-color: #e14eca;
        border-color: #e14eca;
    }

    .btn-primary:not(:disabled):not(.disabled):active,
    .btn-primary:not(:disabled):not(.disabled).active,
    .show>.btn-primary.dropdown-toggle {
        color: #ffffff;
        background-color: #d725bb;
        border-color: #cd23b2;
    }

    .btn-primary:not(:disabled):not(.disabled):active:focus,
    .btn-primary:not(:disabled):not(.disabled).active:focus,
    .show>.btn-primary.dropdown-toggle:focus {
        box-shadow: none, 0 0 0 0 rgba(225, 78, 202, 0.5);
    }

    .btn-link{
        color: white;
    }

    .btn-link:hover {
        color: #cd23b2;
    }
    h3{
        color: #f7f7f7;
        margin-bottom: 20px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="row" align="center">
            <h3>ABBC HYBRID WALLET</h3>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($errors->any())
                        <h4 align="center">{{$errors->first()}}</h4>
                    @endif
                    <p>&nbsp;</p>
                    <form class="form-horizontal" method="POST" action="{{ route('2fa') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="one_time_password" class="col-md-4 control-label">Google OTP Key</label>

                            <div class="col-md-6">
                                <input id="one_time_password" type="number" class="form-control" name="one_time_password" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

