<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::auth();
Route::get('/logout', 'Auth\LoginController@logout');
Route::group(['middleware' => ['auth']], function () {
    /**
     * Main
     */
    Route::get('/', 'PagesController@dashboard');
    Route::get('/user/{user}','UserProfileController@show')->name('user.show');
    Route::get('login/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.login');
    Route::get('login/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.login.callback');
    Route::get('/qrgen','PagesController@qrgen');
    Route::get('dashboard', 'PagesController@dashboard')->name('dashboard');
    Route::POST('dashboard', 'PagesController@dashboard')->name('dashboard');

    Route::get('sendcoin', 'PagesController@sendabbccoin')->name('sendcoin');
    Route::POST('sendcoin', 'PagesController@sendabbccoin')->name('sendcoin');

    Route::get('addresses', 'PagesController@listaddresses')->name('addresses');
    Route::POST('addresses', 'PagesController@listaddresses')->name('addresses');
    Route::get('trans_history', 'PagesController@trans_history')->name('trans_history');
    Route::get('marketcapapi', 'PagesController@marketcapapiprices')->name('marketcapapi');

    Route::get('backupwallet', 'PagesController@backupWallet')->name('backupwallet');
    Route::POST('backupwallet', 'PagesController@backupWallet')->name('backupwallet');

    Route::get('impwallet', 'PagesController@importwallet')->name('impwallet');
    Route::POST('impwallet', 'PagesController@importwallet')->name('impwallet');

    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/destroy/{id}','UsersController@destroy')->name('users.destroy');
    });
    Route::resource('users', 'UsersController');
    Route::get('users/{id}', 'UsersController@edit');
    Route::get('userprofile/{id}', 'UsersController@userprofile');

});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/complete-registration', 'Auth\RegisterController@completeRegistration');

Route::post('/2fa', function () {
    return redirect(URL()->previous());
})->name('2fa')->middleware('2fa');

Route::get('/2fa', function () {
    return redirect('/');
})->name('2fa')->middleware('2fa');

Route::get('/re-authenticate', 'PagesController@reauthenticate');

Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');